﻿using System.IO;
using System.Collections.Generic;

namespace INI
{
	public class IniFile
	{
		public class Section
		{
			string key;
			Dictionary<string, string> values = new Dictionary<string, string>();

			public Section(string key)
			{
				this.key = key;
			}

			public void WriteToFile(StreamWriter writer)
			{
				if (values.Count == 0) return;

				writer.WriteLine("[" + key + "]");
				foreach (KeyValuePair<string, string> pair in values)
				{
					writer.WriteLine(pair.Key + " = " + pair.Value);
				}

				writer.WriteLine();
			}

			public string this[string key]
			{
				get { return values[key]; }
				set
				{
					if (values.ContainsKey(key))
						values[key] = value;

					else values.Add(key, value);
				}
			}

			public override string ToString()
			{
				return "Key = " + key;
			}
		}
		Dictionary<string, Section> sections = new Dictionary<string, Section>();

		public IniFile()
		{
		}

		public IniFile(string filename)
		{
			LoadFrom(filename);
		}

		public void SaveTo(string filename)
		{
			using (StreamWriter str = new StreamWriter(new FileStream(filename, FileMode.Create)))
			{
				foreach (Section s in sections.Values)
					s.WriteToFile(str);
			}
		}

		public void LoadFrom(string filename)
		{
			sections.Clear();

			using (StreamReader str = new StreamReader(new FileStream(filename, FileMode.Open)))
			{
				Section currentSection = null;

				while (!str.EndOfStream)
				{
					string line = str.ReadLine();

					if (line.StartsWith("[") && line.EndsWith("]"))	//SECTION
					{
						string sectionName = line.Substring(1, line.Length - 2);

						currentSection = new Section(sectionName);
						sections.Add(sectionName, currentSection);
					}
					else
					{
						string[] keyValue = line.Split('=');
						if (keyValue.Length != 2 || currentSection == null) continue; //ISNT A KEY-VALUE PAIR

						for(int i=0; i<keyValue.Length; i++)
							keyValue[i] = keyValue[i].Trim(' ');

						currentSection[keyValue[0]] = keyValue[1];
					}
				}
			}
		}

		public Section this[string key]
		{
			get
			{
				if (sections.ContainsKey(key))
					return sections[key];
				else
				{
					Section s = new Section(key);
					sections.Add(key, s);

					return s;
				}
			}
		}
	}
}